class TSPSolver
  # Greedy algoritm implementation
  def solve(initial)
    # We start our jorney from city with index 0
    current_city_index = 0
    path = [current_city_index]

    # We need to visit all cities. As we visited first city, we need to visit
    # `city_count - 1` cities
    i = 0
    while i < initial.length - 1
      # Distances from current city to all others
      distances = initial[current_city_index]

      # 100 is biggest value, should found less then it
      min_value = 100
      min_index = 0

      # Found nearest city that was not visited yet
      distances.each.with_index do |value, index|
        if value < min_value and !path.include? index
          min_value = value
          min_index = index
        end
      end

      # Write nearest city to `path`, set it as current city`
      current_city_index = min_index
      path.push(current_city_index)
      i += 1
    end

    return path
  end
end


data_set = [
  [100, 1, 2, 3],
  [1, 100, 4, 2],
  [2, 4, 100, 1],
  [3, 2, 1, 100]
]

# Another data set for testing
# data_set = [
#   [100, 3, 2, 1],
#   [3, 100, 1, 2],
#   [2, 1, 100, 3],
#   [1, 2, 3, 100]
# ]

solver = TSPSolver.new()
solution = solver.solve(data_set)

puts solution.to_s
